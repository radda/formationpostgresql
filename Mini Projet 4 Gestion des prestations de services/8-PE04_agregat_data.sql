----------------------------------------------------------------------------
--------------- PostgreSQL Exercice 04 : Jointure de tables  ---------------
--- Fonction de base SQL : SELECT, FROM, DISTINCT, LIMIT, AS, WHERE, IN ----
------ INNER JOIN, LEFT OUTER JOIN, LEFT RIGHT JOIN, FULL OUTER JOIN -------
----------------------------------------------------------------------------

/*
Cas pratique
	1/ Afficher le montant TTC des factures par client 
		(Identifiant Client, Nom Client, Prénom Client, Montant TTC)
		Triez le résultat par ordre décroissant de montant TTC

	2/ Afficher la liste des clients à qui nous avons facturé plus 100000 de prestations au cours de l'année 2023
		(Triez le résultat par ordre croissant de montant TTC
		 Afficher les colonnes Identifiant client, Nom Client, Prénom Client, Montant TTC)

	3/ Afficher la dernière date de facturation par client (Triez le résultat par ordre croissant de date)

	4/ Calculer le delai d'inactivité pour chaque (Delai d'inactivité = Date du jour - Dernière date de facturation)
	
	5/ Définir une règle selon le délai d'inactivité : 
		Si le délai d'incactivité est supérieur à 90 jours Alors le client est inactif et dans le cas contraire 
		le client est actif. 
		(Les colonnes à affichier : 
		Identifiant client, Nom Client, Prénom Client, Nombre de jours d'inactivité, Actif/Inactifs)

	6/ Compter le nombre de clients actifs et inactifs

Tester vos connaissances
	7/ Afficher le montant total des règlements de facture pour chaque client et par année
		(Colonne : Identifiant Client, Nom Client, Prénom Client, Total, Année)
		Au niveau de Total, vous devez affichier le montant total des règlements, 
		et au niveau de Année, vous devez afficher le montant réglé pour chaque année.
		Vous devez également calculer le pourentage de règelement par client sur le total des règlements.
		Le résultat doit être trié par ordre décroissant de montant réglé
*/

-- Définir le schéma à utiliser
SET SEARCH_PATH = "OPE";

--- 1/ Afficher le montant TTC des factures par client 
--- (Identifiant client, Nom Client, Prénom Client, Montant TTC)
--- Triez le résultat par ordre décroissant de montant TTC



--- 2/ Afficher la liste des clients à qui nous avons facturé plus 100000 de prestations au cours de l'année 2023
---	 (Triez le résultat par ordre croissant de montant TTC)
---	 (Afficher les colonnes Identifiant client, Nom Client, Prénom Client, Montant TTC)




--- 3/ Afficher la dernière date de facturation par client
--- Triez le résultat par ordre croissant de date




--- 4/ Calculer le delai d'inactivité pour chaque client
--- (Delai d'inactivité = Date du jour - Dernière date de facturation)





--- 5/ Définir une règle selon le délai d'inactivité : 
---		Si le délai d'inactivité est supérieur à 90 jours Alors le client est inactif 
---		et dans le cas contraire 
---		le client est actif. 
---		(Les colonnes à affichier : 
---		Identifiant client, Nom Client, Prénom Client, Nombre de jours d'inactivité, Actif/Inactifs)




--- 6/ Compter le nombre de clients actifs et inactifs


/*Tester vos connaissances
	7/ Afficher le montant total des règlements de facture pour chaque client et par année
		(Colonne : Identifiant Client, Nom Client, Prénom Client, Total, Année)
		Au niveau de Total, vous devez affichier le montant total des règlements, 
		et au niveau de Année, vous devez afficher le montant réglé pour chaque année.
		Vous devez également calculer le pourentage de règelement par client sur le total des règlements.
		Le résultat doit être trié par ordre décroissant de montant réglé
*/







