----------------------------------------------------------------------------
-------- Script de copie des données csv dans les différentes tables -------
----------------------------------------------------------------------------

-- Définir le schéma à utiliser
SET SEARCH_PATH = "OPE";

----------------------------------------------------------------------------
-------------------- Table N°1 : "OPE"."TB_TYPE_CLIENT" --------------------
----------------------------------------------------------------------------

--- Sélection des données présentes dans la table "OPE"."TB_TYPE_CLIENT"
SELECT * FROM "TB_TYPE_CLIENT";

--- Suppression des données présentes dans la table "OPE"."TB_TYPE_CLIENT"
TRUNCATE "TB_TYPE_CLIENT" CASCADE;

---- Copie des données présentes dans le fichier TYPE_CLIENT.csv
COPY  "TB_TYPE_CLIENT"
FROM 'C:\Script SQL - Formation PostgreSQL\Mini Projet 4 Gestion des prestations de services\TYPE_CLIENT.csv'
DELIMITER '|'
CSV HEADER;


----------------------------------------------------------------------------
----------------------- Table N°2 : "OPE"."TB_CLIENT" ----------------------
----------------------------------------------------------------------------

--- Sélection des données présentes dans la table "OPE"."TB_CLIENT"
SELECT * FROM "TB_CLIENT";

--- Suppression des données présentes dans la table "OPE"."TB_CLIENT"
TRUNCATE "TB_CLIENT" CASCADE;

---- Copie des données présentes dans le fichier CLIENT.csv
COPY  "TB_CLIENT"
FROM 'C:\Script SQL - Formation PostgreSQL\Mini Projet 4 Gestion des prestations de services\CLIENT.csv'
DELIMITER '|'
CSV HEADER;


----------------------------------------------------------------------------
-------------------- Table N°3 : "OPE"."TB_PRESTATION" ---------------------
----------------------------------------------------------------------------

--- Sélection des données présentes dans la table "OPE"."TB_PRESTATION"
SELECT * FROM "TB_PRESTATION";

--- Suppression des données présentes dans la table "OPE"."TB_PRESTATION"
TRUNCATE "TB_PRESTATION" CASCADE;

---- Copie des données présentes dans le fichier CLIENT.csv
COPY  "TB_PRESTATION"
FROM 'C:\Script SQL - Formation PostgreSQL\Mini Projet 4 Gestion des prestations de services\PRESTATION.csv'
DELIMITER '|'
CSV HEADER;


----------------------------------------------------------------------------
--------------------- Table N°4 : "OPE"."TB_DEVIS" -------------------------
----------------------------------------------------------------------------
										
--- Sélection des données présentes dans la table "OPE"."TB_DEVIS"
SELECT * FROM "TB_DEVIS";

--- Suppression des données présentes dans la table "OPE"."TB_DEVIS"
TRUNCATE "TB_DEVIS" CASCADE;

---- Copie des données présentes dans le fichier DEVIS.csv
COPY  "TB_DEVIS"
FROM 'C:\Script SQL - Formation PostgreSQL\Mini Projet 4 Gestion des prestations de services\DEVIS.csv'
DELIMITER '|'
CSV HEADER;							
	
										
----------------------------------------------------------------------------
----------------- Table N°5 : "OPE"."TB_DETAIL_DEVIS" ----------------------
----------------------------------------------------------------------------

--- Sélection des données présentes dans la table "OPE"."TB_DETAIL_DEVIS"
SELECT * FROM "TB_DETAIL_DEVIS";

--- Suppression des données présentes dans la table "OPE"."TB_DETAIL_DEVIS"
TRUNCATE "TB_DETAIL_DEVIS" CASCADE;

---- Copie des données présentes dans le fichier DETAIL_DEVIS.csv
COPY  "TB_DETAIL_DEVIS"
FROM 'C:\Script SQL - Formation PostgreSQL\Mini Projet 4 Gestion des prestations de services\DETAIL_DEVIS.csv'
DELIMITER '|'
CSV HEADER;	


----------------------------------------------------------------------------
---------------------- Table N°6 : "OPE"."TB_FACTURE" ----------------------
----------------------------------------------------------------------------

--- Sélection des données présentes dans la table "OPE"."TB_FACTURE"
SELECT * FROM "TB_FACTURE";

--- Suppression des données présentes dans la table "OPE"."TB_FACTURE"
TRUNCATE "TB_FACTURE" CASCADE;

---- Copie des données présentes dans le fichier FACTURE.csv
COPY  "TB_FACTURE"
FROM 'C:\Script SQL - Formation PostgreSQL\Mini Projet 4 Gestion des prestations de services\FACTURE.csv'
DELIMITER '|'
CSV HEADER;


----------------------------------------------------------------------------
----------------- Table N°7 : "OPE"."TB_DETAIL_FACTURE" --------------------
----------------------------------------------------------------------------

--- Sélection des données présentes dans la table "OPE"."TB_DETAIL_FACTURE"
SELECT * FROM "TB_DETAIL_FACTURE";

--- Suppression des données présentes dans la table "OPE"."TB_DETAIL_FACTURE"
TRUNCATE "TB_DETAIL_FACTURE" ;

---- Copie des données présentes dans le fichier DETAIL_FACTURE.csv
COPY  "TB_DETAIL_FACTURE"
FROM 'C:\Script SQL - Formation PostgreSQL\Mini Projet 4 Gestion des prestations de services\DETAIL_FACTURE.csv'
DELIMITER '|'
CSV HEADER;


----------------------------------------------------------------------------
------------------ Table N°8 : "OPE"."TB_TYPE_REGLEMENT" -------------------
----------------------------------------------------------------------------

--- Sélection des données présentes dans la table "OPE"."TB_TYPE_REGLEMENT"
SELECT * FROM "TB_TYPE_REGLEMENT";

--- Suppression des données présentes dans la table "OPE"."TB_TYPE_REGLEMENT"
TRUNCATE "TB_TYPE_REGLEMENT" CASCADE;

---- Copie des données présentes dans le fichier TYPE_REGLEMENT.csv
COPY  "TB_TYPE_REGLEMENT"
FROM 'C:\Script SQL - Formation PostgreSQL\Mini Projet 4 Gestion des prestations de services\TYPE_REGLEMENT.csv'
DELIMITER '|'
CSV HEADER;


----------------------------------------------------------------------------
-------------------- Table N°9 : "OPE"."TB_REGLEMENT" ----------------------
----------------------------------------------------------------------------

--- Sélection des données présentes dans la table "OPE"."TB_REGLEMENT"
SELECT * FROM "TB_REGLEMENT";

--- Suppression des données présentes dans la table "OPE"."TB_REGLEMENT"
TRUNCATE "TB_REGLEMENT" CASCADE;

---- Copie des données présentes dans le fichier REGLEMENT.csv
COPY  "TB_REGLEMENT"
FROM 'C:\Script SQL - Formation PostgreSQL\Mini Projet 4 Gestion des prestations de services\REGLEMENT.csv'
DELIMITER '|'
CSV HEADER;


----------------------------------------------------------------------------
---------------- Table N°10 : "OPE"."TB_DETAIL_REGLEMENT" ------------------
----------------------------------------------------------------------------

--- Sélection des données présentes dans la table "OPE"."TB_DETAIL_REGLEMENT"
SELECT * FROM "TB_DETAIL_REGLEMENT";

--- Suppression des données présentes dans la table "OPE"."TB_DETAIL_REGLEMENT"
TRUNCATE "TB_DETAIL_REGLEMENT" CASCADE;

---- Copie des données présentes dans le fichier DETAIL_REGLEMENT.csv
COPY  "TB_DETAIL_REGLEMENT"
FROM 'C:\Script SQL - Formation PostgreSQL\Mini Projet 4 Gestion des prestations de services\DETAIL_REGLEMENT.csv'
DELIMITER '|'
CSV HEADER;